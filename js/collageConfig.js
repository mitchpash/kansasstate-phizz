var configData = '{"username":"","password":"", "host":"https://api.phizzle.com/rest/", "token":"", "token_valid":""}';
var config = jQuery.parseJSON(configData);

function getToken(callback){



    $.ajax(config.host + "auth/logintokenpe?username="+config.username+"&password="+config.password, {
        dataType:"text",
        success: function(data) {
            config.token = data;

            if(callback != null){
                callback();
            }
            loadQueues();

        },
        error: function(e) {
            document.getElementById('alerts').innerHTML = '';
            addAlert("Invalid username or password");
            showLogin();
            config.token = "";
            config.token_valid  = false;
            console.log('auth error!'+ e	);
        }
    });



}

function showLogin(){
 //   document.getElementById("moderation_form").hidden = true;
    $('#searchresults').hide();
    $('#alerts').html("Queue Authorization Error - Please log in again.")
    document.getElementById("loginform").hidden = false;
    document.getElementById("options").hidden = true;

}

function hideLogin(){

  //  document.getElementById("moderation_form").hidden = false;
    document.getElementById("loginform").hidden = true;
    document.getElementById("options").hidden = false;
}

function setCreds(){
    var username = document.getElementById("username");
    var password = document.getElementById("password");

    document.getElementById('alerts').innerHTML = '';
    if(username == null || username ==  "" || password == null || password == ""){
        addAlert("Invalid username or password");
        showLogin();
        config.token = "";
        config.token_valid  = false;
        return false;
    }



    config.username = username.value;
    config.password = password.value;

    getToken(hideLogin());





}